package com.iot.raspberry.remote.control.domain;

/**
 * Domain Model Class.
 * Represents the possible state of the power switches.
 */
public enum SwitchState {
    ON,
    OFF;
}
